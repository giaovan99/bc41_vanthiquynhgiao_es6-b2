let dataGlasses = [
    { id: "G1", src: "./img/g1.jpg", virtualImg: "./img/v1.png", brand: "Armani Exchange", name: "Bamboo wood", color: "Brown", price: 150, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? " },
    { id: "G2", src: "./img/g2.jpg", virtualImg: "./img/v2.png", brand: "Arnette", name: "American flag", color: "American flag", price: 150, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G3", src: "./img/g3.jpg", virtualImg: "./img/v3.png", brand: "Burberry", name: "Belt of Hippolyte", color: "Blue", price: 100, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G4", src: "./img/g4.jpg", virtualImg: "./img/v4.png", brand: "Coarch", name: "Cretan Bull", color: "Red", price: 100, description: "In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G5", src: "./img/g5.jpg", virtualImg: "./img/v5.png", brand: "D&G", name: "JOYRIDE", color: "Gold", price: 180, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?" },
    { id: "G6", src: "./img/g6.jpg", virtualImg: "./img/v6.png", brand: "Polo", name: "NATTY ICE", color: "Blue, White", price: 120, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G7", src: "./img/g7.jpg", virtualImg: "./img/v7.png", brand: "Ralph", name: "TORTOISE", color: "Black, Yellow", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam." },
    { id: "G8", src: "./img/g8.jpg", virtualImg: "./img/v8.png", brand: "Polo", name: "NATTY ICE", color: "Red, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim." },
    { id: "G9", src: "./img/g9.jpg", virtualImg: "./img/v9.png", brand: "Coarch", name: "MIDNIGHT VIXEN REMIX", color: "Blue, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet." }
];
let stock ='';

let renderGlassList = ()=>{
    var contentHTML='';
    dataGlasses.forEach(item=>{
        var content=`<button id='${item.id}' class='col-4' onclick="dressUp('${item.id}')"><img src="${item.src}"></button>`;
        contentHTML=contentHTML+content;
    });
    document.getElementById('vglassesList').innerHTML=contentHTML;
};

// chạy lần đầu dánh sách kính khi load trang
renderGlassList();

// đeo kính + show thông tin
let dressUp=(id)=>{
    let contentHTML='';
    dataGlasses.forEach(item =>{
        if (item.id==id){
            // gắn giá trị cho stock
            stock=item.id;
            // đeo kính
            document.getElementById('avatar').innerHTML=`<img src="${item.virtualImg}">`;
            // show thông tin
            contentHTML=`
                        <h6>${item.name} - ${item.brand} (${item.color})</h6>
                        <div class="status d-flex">
                            <div class="p-2 bg-danger mr-2 font-weight-bold">$${item.price}</div>
                            <div class="p-2 text-success">Stocking</div>
                        </div>
                        <div>${item.description}</div>             
                        `;
            document.getElementById('glassesInfo').innerHTML=contentHTML;
            document.getElementById('glassesInfo').style.display='block';
        }
    }
    );
};

// remove glass
let removeGlasses=(value)=>{
    // khi dang co kinh deo tren mat nhan vat => stock khac rong
    if (stock!=''){
        var index = (item)=>{return item.id==stock}
        var currentIndex = dataGlasses.findIndex(index);
        if(value){
            if (currentIndex<dataGlasses.length-1){
                dressUp(dataGlasses[currentIndex+1].id);
            }else{
                dressUp(dataGlasses[0].id);
                }
        }else{
            if (currentIndex==0){
                dressUp(dataGlasses[dataGlasses.length-1].id);
            }else{
                dressUp(dataGlasses[currentIndex-1].id);
                }
        }
    }
    else{
        if(value){
            // khi chưa đeo kinh nào (stock==''), mặc định after == id1
            dressUp(dataGlasses[0].id); 
        }else{
            // khi chưa đeo kinh nào (stock==''), mặc định before == id cuối cùng
            console.log(dataGlasses);
            dressUp(dataGlasses[dataGlasses.length-1].id)
        };
    };
};
